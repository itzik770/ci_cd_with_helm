import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    console.log("NODE_ENV", process.env.NODE_ENV);
    console.log("ENV", process.env.PORT);
    
    return this.appService.getHello();
  }
}
