import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    console.log("NODE_ENV", process.env.NODE_ENV);
    console.log("ENV", process.env.PORT);
    
    return 'Hello World!';
  }
}
